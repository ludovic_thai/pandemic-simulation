set xlabel "Time"
set ylabel "Numbers of citizens"
set key outside
set style fill transparent solid 1 noborder
plot "evolution.txt" u 1:2 w lines t "Healthy", "evolution.txt" u 1:3 w lines t "Sick", "evolution.txt" u 1:4 w lines t "Dead", "evolution.txt" u 1:5 w lines t "Burned"