CC=gcc
INC=-I./include -I./include/display_shell 
CFLAGS=-Wall -Wextra -pedantic -c -fms-extensions $(INC) -g -lrt
LDFLAGS= -g -lpthread -lncurses -lrt
OBJ_PATH=./obj
OBJS= ./obj/cell.o ./obj/citizen.o ./obj/city.o ./obj/coordinates.o ./obj/field.o ./obj/general_informations.o ./obj/display_functions.o ./obj/shared_memory.o ./obj/pipes.o ./obj/citizen_threads.o ./obj/shell.o ./obj/signal_handler.o ./obj/news.o
MAIN_INCLUDES= ./include/cell.h ./include/citizen.h ./include/city.h ./include/coordinates.h ./include/field.h ./include/general_informations.h ./include/global.h ./include/display_shell/display_functions.h ./include/shared_memory.h ./include/pipes.h ./include/citizen_threads.h ./include/display_shell/shell.h ./include/signal_handler.h ./include/news.h
EXEC=./bin/epidemic_sim.bin ./bin/timer.bin ./bin/citizen_manager.bin ./bin/press_agency.bin

.PHONY : all clean distclean doc

all: $(EXEC) 

test: ./bin/test_thai.bin ./bin/test_rotger.bin

doc:
	cd doc/latex/; make clean
	rm -rf doc/*.pdf
	cd doc/latex/; make
	mv doc/latex/report.pdf doc/report.pdf
	cd doc/latex/; make clean

./bin/epidemic_sim.bin: ./obj/epidemic_sim.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./bin/test_thai.bin: ./obj/test_thai.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./bin/test_rotger.bin: ./obj/test_rotger.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./obj/epidemic_sim.o: src/epidemic_sim.c $(MAIN_INCLUDES)
	$(CC) $(CFLAGS) $< -o $@

./obj/test_thai.o: ./src/test_thai.c $(MAIN_INCLUDES)
	$(CC) $(CFLAGS) $< -o $@

./obj/test_rotger.o: ./src/test_rotger.c $(MAIN_INCLUDES)
	$(CC) $(CFLAGS) $< -o $@

./obj/display_functions.o: ./src/display_shell/display_functions.c ./include/display_shell/display_functions.h
	$(CC) $(CFLAGS) $< -o $@

./obj/shell.o: ./src/display_shell/shell.c ./include/display_shell/shell.h
	$(CC) $(CFLAGS) $< -o $@

./obj/cell.o: ./src/cell.c ./include/cell.h ./include/city.h
	$(CC) $(CFLAGS) $< -o $@

./obj/citizen.o: ./src/citizen.c ./include/citizen.h ./include/coordinates.h
	$(CC) $(CFLAGS) $< -o $@

./obj/city.o: ./src/city.c ./include/city.h ./include/citizen.h ./include/cell.h
	$(CC) $(CFLAGS) $< -o $@

./obj/coordinates.o: ./src/coordinates.c ./include/coordinates.h
	$(CC) $(CFLAGS) $< -o $@

./obj/field.o: ./src/field.c ./include/field.h 
	$(CC) $(CFLAGS) $< -o $@

./obj/general_informations.o: ./src/general_informations.c ./include/general_informations.h
	$(CC) $(CFLAGS) $< -o $@

./obj/shared_memory.o: ./src/shared_memory.c ./include/shared_memory.h
	$(CC) $(CFLAGS) $< -o $@

./obj/timer.o: ./src/timer.c ./include/shared_memory.h
	$(CC) $(CFLAGS) $< -o $@

./bin/timer.bin: ./obj/timer.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./obj/pipes.o: ./src/pipes.c ./include/pipes.h
	$(CC) $(CFLAGS) $< -o $@

./obj/citizen_threads.o: ./src/citizen_threads.c ./include/citizen_threads.h ./include/city.h
	$(CC) $(CFLAGS) $< -o $@

./obj/citizen_manager.o: ./src/citizen_manager.c ./include/shared_memory.h
	$(CC) $(CFLAGS) $< -o $@

./bin/citizen_manager.bin: ./obj/citizen_manager.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./obj/signal_handler.o: ./src/signal_handler.c ./include/signal_handler.h
	$(CC) $(CFLAGS) $< -o $@

./bin/press_agency.bin: ./obj/press_agency.o $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS)

./obj/press_agency.o: ./src/press_agency.c include/news.h
	$(CC) $(CFLAGS) $< -o $@

./obj/news.o: ./src/news.c include/news.h
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf ./obj/*.o
	rm -rf ./src/*~
	rm -rf *~
	rm -rf evolution.txt
	rm -rf *.DS_Store
	rm -rf *pipe*
	cd doc/latex/; make clean

distclean: clean
	rm -rf ./bin/*.bin
