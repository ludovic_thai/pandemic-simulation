<!--suppress HtmlRequiredAltAttribute -->
<a href="https://www.ensicaen.fr">
<img src="https://www.ensicaen.fr/wp-content/uploads/2017/02/LogoEnsicaen.gif" width="256" >
</a>

OPERATING SYSTEM PROJECT : Plague Simulation
=============================================
Olivier ROTGER - orotger@ecole.ensicaen.fr \
Ludovic THAI - ludovic.thai@ecole.ensicaen.fr \
Benjamin VIGNAUX - bvignaux@ecole.ensiaen.fr

# Project description
This project is made of 4 programs that simulates the propagation of a plague inside a city. \
There 4 types of citizen who live in the city:
- *Civilian*
- *Doctors* who are able to heal other citizens
- *Firefighter* who are able to burn deaths people and to decontaminate places
- *Reporters* who send reports to an press agency located outside the city

The city is a 7x7 map that contains 4 types of terrain: 
- *Wasteland*
- *Firehouse* where firefighter can get decontamination material and where citizen can reduce their contamination level 
- *Hospital* where doctors can get medecine to heal other citizen and where people can be treated for the plague
- *Dwelling*

The press agency aims to inform the population. However, it applies a policy of censorship by displaying reduce numbers to avoid panic. \
The simulation lasts 100 days. 


# Installation and execution

## Installation on _Linux_
```
make 
``` 
You will need a colored terminal to run the application.:
````
export TERM=xterm-256color
````
You may also need to install `ncurses`:
```
apt-get install libncurses5-dev libncursesw5-dev
```

## Installation on _MacOS_
This program don't work of MacOS because it needs POSIX Memory Queue which doesn't exist on this OS.

## Execution
- Set your terminal in **full screen**.
- Do **not leave the shells** before the end.\

Run in order in **4 seperated shells**:
- The program that handles the city:
```
./bin/epidemic_sim.bin
```
This program displays the evolution of the city day by day.
- The program that handles the press agency:
```
./bin/press_agency.bin
```
This program displays the news released by the press agency if the agency receives reports from its reporters. 
- The program that handles the threads:
```
./bin/citizen_manager.bin
```
- The program that handles the time:
```
./bin/timer.bin 3
```
The `3` is for _3 seconds_ between each round.

## Clean Directory
````
make clean
make distclean
````

# Report
The report is located at : `./doc/report.pdf` \
If necessary, you can generate the report using the command: 
```
make doc
```
