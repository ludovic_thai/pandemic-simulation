/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef __SHARED_MEMORY_H
#define __SHARED_MEMORY_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "city.h"

#define MEMORY_PATH "/shm_name"
#define MAC_OSX 0

void handle_error(char *message);
void handle_sigint(int signum);
city_t* open_server();
city_t* open_client();
void unlink_memory();

#endif