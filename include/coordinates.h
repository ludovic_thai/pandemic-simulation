/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef PLAGUESIMULATION_COORDINATES_T_H
#define PLAGUESIMULATION_COORDINATES_T_H

typedef struct coordinates coordinates_t;
struct coordinates {
    int pos_x;
    int pos_y;
};

coordinates_t new_coordinates(int x, int y);

int equals(coordinates_t pos1, coordinates_t pos2);

int is_in_map_boundaries(coordinates_t pos);

#endif
