/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#ifndef __GLOBAL_H
#define __GLOBAL_H

#define MAP_SIZE 7

#define NUMBER_OF_HOUSES 12
#define NUMBER_OF_WASTELANDS 34
#define NUMBER_OF_FIREHOUSES 2
#define NUMBER_OF_HOSPITALS 1

#define SIMULATION_TIME 100

#define POPULATION 37
#define NUMBER_OF_DOCTORS 4
#define NUMBER_OF_FIREFIGHTERS 6
#define NUMBER_OF_CIVILIANS 25
#define NUMBER_OF_REPORTERS 2

#define WASTELAND_CAPACITY 16
#define FIREHOUSE_CAPACITY 8
#define HOSPITAL_CAPACITY 12
#define HOUSE_CAPACITY 6
#define MAX_CAPACITY 16

#define EVER ;;

#endif
