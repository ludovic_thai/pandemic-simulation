/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef _SHELL_H
#define _SHELL_H

#include "city.h"
#include <ncurses.h>

#define FORE_WHITE 1
#define DWELLING_COLOR 2
#define HOSPITAL_COLOR 3
#define WASTELAND_COLOR 4
#define FIREHOUSE_COLOR 5

#define CONTAMINATION_LVL1 6
#define CONTAMINATION_LVL2 7
#define CONTAMINATION_LVL3 8
#define CONTAMINATION_LVL4 9
#define CONTAMINATION_LVL5 10

#define BASE_X_PLACES 15
#define BASE_Y_PLACES 5

#define BASE_X_CONT 50
#define BASE_Y_CONT 5

#define BASE_X_POP 85
#define BASE_Y_POP 5

WINDOW *init_places_map();

void refresh_places_map(city_t *city, WINDOW *box);

WINDOW *init_contamination_map();

void refresh_contamination_map(city_t *city, WINDOW *box);

WINDOW *init_population_map();

void refresh_population_map(city_t *city, WINDOW *box);

void refresh_repartition(city_t *city);

void refresh_shell(city_t *city);

#endif
