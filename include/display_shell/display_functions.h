/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "city.h"

#ifndef __DISPLAY_FUNCTIONS_H
#define __DISPLAY_FUNCTIONS_H

typedef struct data_cell {
    char lines[4][4];
} data_cell_t;

void display_map(char **map);

char **city_to_map(city_t city);

void display_city_all(city_t city);

void display_city_population(city_t city);

void display_city_parameters(city_t city);

data_cell_t **population_to_data_cell(city_t city);

void display_data(data_cell_t **data);

char *parse_integer(int value);

int count_digits(int value);

data_cell_t **city_parameters_to_data_cell(city_t city);

#endif
