/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef __FIELD_H
#define __FIELD_H

typedef enum {
    FIREHOUSE,
    HOSPITAL,
    HOUSE,
    WASTELAND,
    NONE
} field_type;

typedef struct {
    field_type field;
    float contamination_level;
    int capacity;
} field_t;

field_t new_field();

field_t init_field(field_type type_of_field);

int field_capacity(field_type type_of_field);

void display_field_type(field_t field);

void display_field(field_t field);

field_t init_contamination_level(field_t field);

#endif
