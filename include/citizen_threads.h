/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */



#ifndef __CITIZEN_THREADS_H
#define __CITIZEN_THREADS_H

#include "city.h"

typedef struct thread_param thread_param_t;
struct thread_param {
    city_t *city;
    int citizen_id;
};

void *handle_citizen(void *args);

citizen_t move_citizen(city_t city, int citizen_id, coordinates_t new_pos);

int is_allow(citizen_t citizen, cell_t cell);

coordinates_t new_random_pos(citizen_t citizen, city_t city);

void update_population(thread_param_t *param, coordinates_t previous_pos,
        coordinates_t new_pos, citizen_type type);

void add_citizen(thread_param_t *param, coordinates_t coordinates, int id);

void pop_citizen(thread_param_t *param, coordinates_t coordinates, int id);

void update_citizen(thread_param_t *param, int citizen_id, int same_cell);

void update_contamination(thread_param_t *param, int citizen_id, int same_cell);

void update_citizen_contamination(thread_param_t *param, citizen_t
citizen, int same_cell, float field_contamination_level);

void update_field_contamination(thread_param_t *param, cell_t cell, float
citizen_contamination_level);

void update_citizen_state(thread_param_t *param, int citizen_id);

int healthy_to_sick(float contamination_level);

int sick_to_dead(float death_probability);

int has_doctor_on_cell(thread_param_t *param, coordinates_t pos);

void contaminate_other_citizens(thread_param_t* param, coordinates_t
coordinates);

void burn_dead_people(thread_param_t* param, coordinates_t coordinates);

void decontaminate(thread_param_t* param, citizen_t citizen);

void heal_citizen(thread_param_t* param, citizen_t citizen);

#endif
