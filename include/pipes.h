/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#ifndef __PIPES_H
#define __PIPES_H

#define TIMER_PIPE "timer_pipe"
#define SIM_PIPE "sim_pipe"

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

void kill_pipe(char* name);

#endif
