/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef __CELL_H
#define __CELL_H


#include "field.h"
#include "coordinates.h"
#include "global.h"

typedef struct {
    coordinates_t coordinates;
    field_t field;
    int firefighters;
    int doctors;
    int reporters;
    int civilians;
    int ids[MAX_CAPACITY];
} cell_t;

cell_t new_cell(coordinates_t coordinates);

cell_t init_cell_field(cell_t cell, field_type type_of_field);

void display_cell(cell_t cell);

int is_full(cell_t cell);

int get_number_of_citizens(cell_t cell);



#endif