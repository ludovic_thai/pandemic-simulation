/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef PLAGUESIMULATION_NEWS_H
#define PLAGUESIMULATION_NEWS_H

#include <mqueue.h>
#include <string.h>
#include <assert.h>

#include "citizen_threads.h"

#define MQUEUE_NAME "/news"
#define MAX_SIZE 8192
#define MSG_STOP "exit"
#define MAX_MESSAGE 10

typedef enum {
  AVG_CONT_LVL,
  NB_SICK,
  DEATHS,
  REPORTER_CONT_LVL,
  ERROR
} message_type ;

typedef struct{
  long day;
  long reporter_id;
  message_type message_type;
  float value;
} message_t;

mqd_t new_mqueue();

void send_reports(thread_param_t* param, citizen_t citizen); 

message_t parse_message(char* message);

char** str_split(char* a_str, const char a_delim);

void display_message(message_t message);
#endif 
