/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#ifndef _SIGNAL_HANDLER_H
#define _SIGNAL_HANDLER_H

#include "pipes.h"
#include "shared_memory.h"
#include <ncurses.h>

void handle_sigint(int signum);

void handle_sigusr(int signum);

void handle_sigchld(int signum);

#endif
