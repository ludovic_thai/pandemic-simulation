/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef CITIZEN_T_H
#define CITIZEN_T_H

#include "coordinates.h"

typedef enum {
    DOCTOR,
    FIREFIGHTER,
    REPORTER,
    CIVILIAN
} citizen_type;

typedef enum {
    DEAD,
    HEALTHY,
    SICK,
    BURNED
} life_status;

typedef struct {
    citizen_type type;
    coordinates_t position;
    int id;
    float contamination_level;
    life_status status;
    int contamination_day;
    int healing_day;
    int last_entry_in_hospital;
    float death_probability;
    float spray_power;
    int care_package;

} citizen_t;

citizen_t new_citizen(citizen_type type, coordinates_t position, int id);
void display_citizen_type(citizen_t citizen);
void display_life_status(citizen_t citizen);
void display_skills(citizen_t citizen);
void display_citizen(citizen_t citizen);

#endif
