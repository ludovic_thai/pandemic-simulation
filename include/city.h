/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef __CITY_H
#define __CITY_H

#include "cell.h"
#include "citizen.h"
#include "global.h"
#include <stdlib.h>
#include <stdio.h>
#include "time.h"
#include "field.h"
#include "general_informations.h"

typedef struct city city_t;
struct city {
  int time;
  cell_t map[MAP_SIZE][MAP_SIZE];
  citizen_t citizens[POPULATION];
  general_informations_t general_informations;
};

city_t new_city();

void display_city(city_t city);

city_t place_hospital(city_t city);

city_t place_firehouses(city_t city);

city_t place_houses(city_t city, int houses);

city_t place_wastelands(city_t city);

city_t init_fields(city_t city, int houses);

city_t init_wasteland_contamination(city_t city, int nb_wastelands);

city_t place_citizens(city_t city, int nb_doctors, int nb_firefighters,
        int nb_civilians, int nb_reporters);

city_t add_citizen_to_city(city_t city, cell_t cell, citizen_t citizen);

city_t init_city();

city_t decontaminate_citizen(city_t city);

city_t wind_driven_propagation(city_t city);

city_t update_neighbours_contamination(city_t city, coordinates_t coordinates);

city_t increase_contamination(city_t city, coordinates_t coordinates,
        float contamination_difference);

int number_of_healthy(city_t city);

int number_of_sick(city_t city);

int number_of_dead(city_t city);

int number_of_burned(city_t city);

float average_contamination_level(city_t);
  
void update_general_informations(city_t *city);
  
#endif
