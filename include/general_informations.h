/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
2 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#ifndef PLAGUESIMULATION_GENERAL_INFORMATIONS_H
#define PLAGUESIMULATION_GENERAL_INFORMATIONS_H

typedef struct {
    int contaminated_citizens;
    float average_contamination_level;
    int death_citizens;
} general_informations_t;

general_informations_t* new_general_informations();

#endif
