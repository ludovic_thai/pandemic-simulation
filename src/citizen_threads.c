/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pipes.h>
#include "news.h"
#include "citizen_threads.h"

// Function called on thread creation
void *handle_citizen(void *args)
{
        thread_param_t *param = (thread_param_t *) args;
        int citizen_id;
        coordinates_t previous_pos;
        coordinates_t new_pos;
        int fd_sim;
        char input;

        // Opening timer_pipe in read mode
        fd_sim = open(SIM_PIPE, O_RDONLY);
        if (fd_sim == -1) {
                perror("Pipe isn't available");
        }


        while (read(fd_sim, &input, 1) > 0) {
                if (input == '1') {
                        citizen_id = param->citizen_id;
                        citizen_t citizen = param->city->citizens[citizen_id];
                        if (citizen.status != DEAD &&
                            citizen.status != BURNED) {
                                previous_pos = param->city->
                                        citizens[citizen_id].position;
                                new_pos = new_random_pos(citizen, *param->city);
                                param->city->citizens[citizen_id] =
                                        move_citizen(
                                                *param->city,
                                                citizen_id,
                                                new_pos);
                                add_citizen(param, new_pos, citizen_id);
                                pop_citizen(param, previous_pos, citizen_id);
                                update_citizen(param, citizen_id,
                                               equals(previous_pos, new_pos));
                                update_contamination(param, citizen_id,
                                                     equals(previous_pos,
                                                            new_pos));
                                update_citizen_state(param, citizen_id);

                        }
                        if (citizen.status == SICK || citizen.status == DEAD) {
                                contaminate_other_citizens(param,
                                                           citizen.position);
                        }
                        if (citizen.status != DEAD &&
                            citizen.status != BURNED) {
                                switch (citizen.type) {
                                        case DOCTOR:
                                                heal_citizen(param, citizen);
                                                break;
                                        case FIREFIGHTER:
                                                burn_dead_people(param,
                                                        citizen.position);
                                                decontaminate(param, citizen);
                                                break;
                                        case REPORTER:
                                                send_reports(param, citizen);
                                                break;
                                        case CIVILIAN:
                                                break;
                                }
                        }
                } else {
                        exit(EXIT_SUCCESS);
                }
                sleep(1);

        }

        return NULL;
}

citizen_t move_citizen(city_t city, int citizen_id, coordinates_t new_pos)
{
        citizen_t citizen = city.citizens[citizen_id];
        if (equals(new_pos, citizen.position)) {
                return citizen;
        } else {
                citizen.position = new_pos;
                return citizen;
        }
}

int is_allow(citizen_t citizen, cell_t cell)
{
        field_type type = cell.field.field;
        switch (type) {
                case HOSPITAL:
                        if (citizen.status == SICK || citizen.type == DOCTOR ||
                            citizen
                                    .type == FIREFIGHTER) {
                                return 1;
                        }
                        break;
                case FIREHOUSE:
                        if (citizen.type == FIREFIGHTER ||
                            (citizen.type != FIREFIGHTER
                             && cell.firefighters > 0)) {
                                return 1;
                        }
                        break;
                default:
                        return 1;
        }
        return 0;
}

coordinates_t new_random_pos(citizen_t citizen, city_t city)
{
        srand(time((NULL)));
        coordinates_t new_pos;
        coordinates_t previous_pos = citizen.position;
        int x, y;
        int i, j;
        x = previous_pos.pos_x;
        y = previous_pos.pos_y;
        if (rand() % 100 <= 40) {
                sleep(1);
                do {
                        i = rand() % 3 - 1;
                        j = rand() % 3 - 1;
                        new_pos = new_coordinates(x + i, y + j);
                } while (!is_in_map_boundaries(new_pos) ||
                         !is_allow(citizen, city
                                 .map[x + i][y + j]));
                return new_pos;
        } else {
                return previous_pos;
        }
}

void add_citizen(thread_param_t *param, coordinates_t coordinates, int id)
{
        cell_t cell = param->city->map[coordinates.pos_x][coordinates.pos_y];
        switch (param->city->citizens[id].type) {
                case FIREFIGHTER:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        firefighters++;
                        break;
                case DOCTOR:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        doctors++;
                        break;
                case REPORTER:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        reporters++;
                        break;
                case CIVILIAN:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        civilians++;
                        break;
                default:
                        break;
        }
        for (int i = 0; i < cell.field.capacity; i++) {
                if (cell.ids[i] == -1) {
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        ids[i] = id;
                        break;
                }
        }
}

void pop_citizen(thread_param_t *param, coordinates_t coordinates, int id)
{
        cell_t cell = param->city->map[coordinates.pos_x][coordinates.pos_y];
        switch (param->city->citizens[id].type) {
                case FIREFIGHTER:
                        param->city->map[coordinates.pos_x][coordinates.pos_y]
                                .firefighters--;
                        break;
                case DOCTOR:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        doctors--;
                        break;
                case REPORTER:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        reporters--;
                        break;
                case CIVILIAN:
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        civilians--;
                        break;
                default:
                        break;
        }
        for (int i = 0; i < cell.field.capacity; i++) {
                if (cell.ids[i] == id) {
                        param->city->map[coordinates.pos_x][coordinates.pos_y].
                        ids[i] = -1;
                        break;
                }
        }
}

void update_citizen(thread_param_t *param, int citizen_id, int same_cell)
{
        citizen_type type = param->city->citizens[citizen_id].type;
        coordinates_t pos = param->city->citizens[citizen_id].position;
        field_type field = param->city->map[pos.pos_x][pos.pos_y].field.field;

        if (!same_cell) {
                switch (type) {
                        case DOCTOR:
                                if (field == HOSPITAL) {
                                        param->city->citizens[citizen_id].
                                        care_package += 10;
                                }
                                break;
                        case FIREFIGHTER:
                                if (field == FIREHOUSE) {
                                        param->city->citizens[citizen_id].
                                        spray_power = 10;
                                }
                                break;
                        default:
                                break;
                }
        }
}


void
update_contamination(thread_param_t *param, int citizen_id, int same_cell)
{
        citizen_t citizen = param->city->citizens[citizen_id];
        cell_t cell = param->city->map[citizen.position.pos_x]
                [citizen.position.pos_y];
        float citizen_contamination_level = citizen.contamination_level;
        float field_contamination_level = cell.field.contamination_level;
        update_citizen_contamination(param, citizen, same_cell,
                                     field_contamination_level);
        update_field_contamination(param, cell, citizen_contamination_level);
}

void update_citizen_contamination(thread_param_t *param, citizen_t
citizen, int same_cell, float field_contamination_level)
{
        float contamination_factor;

        if (same_cell == 0) {
                contamination_factor = 0.02f;
        } else {
                contamination_factor = 0.05f;
        }

        if (citizen.type == FIREFIGHTER) {
                contamination_factor = 0.1f * contamination_factor;
        }

        citizen.contamination_level += contamination_factor *
                                       field_contamination_level;

        if (citizen.contamination_level > 1) {
                citizen.contamination_level = 1;
        }

        param->city->citizens[citizen.id] = citizen;
}

void update_field_contamination(thread_param_t *param, cell_t cell, float
citizen_contamination_level)
{
        if (cell.field.field != FIREHOUSE) {
                if (cell.field.field == HOSPITAL) {
                        param->city->map[cell.coordinates.pos_x]
                        [cell.coordinates.pos_y]
                                .field.contamination_level +=
                                0.01f * citizen_contamination_level
                                / 4;
                } else {
                        param->city->map[cell.coordinates.pos_x]
                        [cell.coordinates.pos_y]
                                .field.contamination_level +=
                                0.01f * citizen_contamination_level;
                }

                if (param->city->map[cell.coordinates.pos_x]
                [cell.coordinates.pos_y]
                            .field.contamination_level > 1) {
                        param->city->map[cell.coordinates.pos_x]
                        [cell.coordinates.pos_y]
                                .field.contamination_level = 1;
                }
        }
}


void update_citizen_state(thread_param_t *param, int citizen_id)
{
        citizen_t citizen = param->city->citizens[citizen_id];
        int sick_days;
        int dead;
        switch (citizen.status) {
                case HEALTHY:
                        if (healthy_to_sick(citizen.contamination_level)) {
                                citizen.status = SICK;
                                citizen.contamination_day = param->city->time;
                        }
                        break;
                case SICK :
                        sick_days =
                                param->city->time - citizen.contamination_day;
                        if (sick_days > 5) {
                                citizen.death_probability += 0.05f;
                        }
                        if (param->city->map[citizen.position.pos_x]
                        [citizen.position.pos_y].field.field == HOSPITAL) {
                                dead = sick_to_dead(
                                        citizen.
                                        death_probability / 4);
                        } else if (has_doctor_on_cell(param,
                                                      citizen.position)) {
                                dead = sick_to_dead(
                                        citizen.
                                        death_probability / 2);
                        } else {
                                dead = sick_to_dead(citizen.death_probability);
                        }
                        if (dead) {
                                citizen.status = DEAD;
                                citizen.contamination_day = -1;
                        }
                        break;
                default:
                        break;
        }
        param->city->citizens[citizen_id] = citizen;
}

int healthy_to_sick(float contamination_level)
{
        int x;
        x = rand();
        return x % 100 < contamination_level * 100;
}

int sick_to_dead(float death_probability)
{
        int x;
        x = rand();
        return x % 100 < death_probability * 100;
}

int has_doctor_on_cell(thread_param_t *param, coordinates_t pos)
{
        int *ids;
        int i;
        ids = param->city->map[pos.pos_x][pos.pos_y].ids;
        for (i = 0; i < MAX_CAPACITY; ++i) {
                if (param->city->citizens[ids[i]].type == DOCTOR) {
                        return 1;
                }
        }
        return 0;
}

void contaminate_other_citizens(thread_param_t *param, coordinates_t
coordinates)
{
        if (param->city->map[coordinates.pos_x][coordinates.pos_y].field.
        field == WASTELAND) {
                for (int i = -1; i < 2; ++i) {
                        for (int j = -1; j < 2; ++j) {
                                coordinates_t new_coord = new_coordinates(
                                        coordinates
                                                .pos_x + i,
                                        coordinates.pos_y + j);
                                if (is_in_map_boundaries(new_coord) &&
                                    param->city->map[new_coord.pos_x]
                                    [new_coord.pos_y].field
                                            .field == WASTELAND) {
                                        for (int i = 0; i < POPULATION; ++i) {
                                                citizen_t citizen_target
                                                = param->city->citizens[i];
                                                if (equals(new_coord,
                                                           citizen_target.
                                                           position) &&
                                                    citizen_target
                                                            .status ==
                                                    HEALTHY) {
                                                        float chance = 0.01f;
                                                        if (citizen_target.type
                                                        == FIREFIGHTER) {
                                                                chance = 0.003f;
                                                        }
                                                        if (healthy_to_sick(
                                                                chance)) {
                                                                param->city->
                                                                citizens[i].
                                                                status = SICK;
                                                                param->city->
                                                                citizens[i].
                                                                contamination_day
                                                                = param->city->
                                                                        time;
                                                        }
                                                }
                                        }
                                }
                        }
                }
        } else {
                for (int i = 0; i < POPULATION; ++i) {
                        citizen_t citizen_target = param->city->citizens[i];
                        if (equals(coordinates, citizen_target.position) &&
                            citizen_target
                                    .status == HEALTHY) {
                                float chance = 0.1f;
                                if (citizen_target.type == FIREFIGHTER) {
                                        chance = 0.03f;
                                }
                                if (healthy_to_sick(chance)) {
                                        param->city->citizens[i].status = SICK;
                                        param->city->citizens[i].
                                        contamination_day = param->city->time;
                                }
                        }
                }
        }
}

void burn_dead_people(thread_param_t *param, coordinates_t coordinates)
{
        for (int i = 0; i < POPULATION; ++i) {
                if (equals(coordinates, param->city->citizens[i].position) &&
                    param
                            ->city->citizens[i].status ==
                    DEAD) {
                        param->city->citizens[i].status = BURNED;
                }
        }
}

float min(float num1, float num2)
{
        if (num1 < num2) {
                return num1;
        } else {
                return num2;
        }
}

void decontaminate(thread_param_t *param, citizen_t citizen)
{
        float used_spray = 0;
        float max_spray = min(1, citizen.spray_power);

        for (int i = 0; i < POPULATION; ++i) {
                citizen_t contaminate_citizen = param->city->citizens[i];
                if (equals(contaminate_citizen.position, citizen.position)) {
                        float to_decontaminate = min(0.2f,
                                                     min(max_spray -
                                                     used_spray,
                                                         contaminate_citizen.
                                                         contamination_level));
                        used_spray += to_decontaminate;
                        param->city->citizens[i].contamination_level -=
                                to_decontaminate;
                }
        }

        float to_decontaminate = min(max_spray - used_spray,
                                     param->city->map[citizen.position.pos_x]
                                     [citizen.position.pos_y]
                                             .field.contamination_level);
        param->city->map[citizen.position.pos_x][citizen.position.pos_y]
                .field.contamination_level -= to_decontaminate;
}

void heal_citizen(thread_param_t *param, citizen_t citizen)
{
        if (citizen.status == SICK) {
                if (param->city->time - citizen.contamination_day <= 10) {
                        if (param->city->map[citizen.position.pos_x]
                        [citizen.position
                                .pos_y].field.field == HOSPITAL) {
                                param->city->citizens->status = HEALTHY;
                                param->city->citizens->contamination_day = -1;
                        } else if (citizen.care_package > 0) {
                                param->city->citizens[citizen.id].status =
                                        HEALTHY;
                                param->city->citizens[citizen.id].
                                contamination_day = -1;
                                param->city->citizens[citizen.id].
                                care_package--;
                        }
                }
        } else {
                float contamination_level = 0;
                int citizen_id = -1;

                for (int i = 0; i < POPULATION; ++i) {
                        if (equals(citizen.position,
                                   param->city->citizens[i].position) &&
                            param->city->citizens[i].status == SICK) {
                                if (param->city->citizens[i].contamination_level
                                > contamination_level) {
                                        contamination_level = param->city->
                                                citizens[i].contamination_level;
                                        citizen_id = i;
                                }
                        }
                }
                if (citizen_id != -1) {
                        if (param->city->map[citizen.position.pos_x]
                        [citizen.position
                                .pos_y].field.field == HOSPITAL) {
                                param->city->citizens[citizen_id].status =
                                        HEALTHY;
                                param->city->citizens[citizen_id].
                                contamination_day = -1;
                        } else if (citizen.care_package > 0) {
                                param->city->citizens[citizen_id].status =
                                        HEALTHY;
                                param->city->citizens[citizen_id].
                                contamination_day = -1;
                                param->city->citizens[citizen.id].
                                care_package--;
                        }
                }
        }
}

