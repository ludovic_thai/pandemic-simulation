/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <mqueue.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "shared_memory.h"
#include "display_functions.h"
#include "city.h"
#include "pipes.h"
#include "shell.h"
#include "news.h"
#include "signal_handler.h"

int main() {
    char input;
    int fd_timer;
    int fd_sim;
    int i;
    FILE *p_file;


    // Creating timer_pipe file
    kill_pipe(TIMER_PIPE);
    if (mkfifo(TIMER_PIPE, 0666) != 0) {
        perror("Error creating named pipe");
    }

    // Creating sim_pipe file
    kill_pipe(SIM_PIPE);
    if (mkfifo(SIM_PIPE, 0666) != 0) {
        perror("Error creating named pipe");
    }

    pid_t pid = fork();
    if (pid > 0) {
        // Father process

	// Creating memory queue for news
	mqd_t mq;
	mq = new_mqueue();
	
	// Initialize shared memory
        city_t *shm = open_server();
  
        // Opening timer_pipe in read mode
        fd_timer = open(TIMER_PIPE, O_RDONLY);
        if (fd_timer == -1) {
            perror("Pipe isn't available");
        }

        // Opening sim_pipe in write mode
        fd_sim = open(SIM_PIPE, O_WRONLY);
        if (fd_sim == -1) {
            perror("Pipe isn't available");
        }


        // Create sigaction for SIGCHILD signal to avoid zombie
        struct sigaction action_sigchild;
        action_sigchild.sa_handler = &handle_sigchld;
        sigaction(SIGCHLD, &action_sigchild, NULL);


        // Opening population's data file
        p_file = fopen("evolution.txt", "w");

        initscr();
        cbreak();
        noecho();

        while (read(fd_timer, &input, 1) > 0) {

            if (input == '1') {

                // New round actions
                refresh_shell(shm);

                *shm = wind_driven_propagation(*shm);
                *shm = decontaminate_citizen(*shm);

                fprintf(p_file, "%d %d %d %d %d\n", shm->time,
                        number_of_healthy(*shm), number_of_sick(*shm),
                        number_of_dead(*shm),
                        number_of_burned(*shm));

                // Send a signal to citizen_manager threads
                // that a new round started
                for (i = 0; i < POPULATION; ++i) {
                    write(fd_sim, "1", 1);
                }


		// Update city's general information
		update_general_informations(shm);
            } else {

                endwin();

                // Send a signal to citizen_manager threads to end them
                for (i = 0; i < POPULATION; ++i) {
                    write(fd_sim, "0", 1);
                }

                // Close file and pipes
                fclose(p_file);
                close(fd_timer);
                close(fd_sim);

                // Close and unlink the memory queue
                mq_close(mq);
                mq_unlink(MQUEUE_NAME);
		
                // Create sigaction for SIGCHILD signal to avoid zombie
                struct sigaction action_sigchild;
                action_sigchild.sa_handler = &handle_sigchld;
                sigaction(SIGCHLD, &action_sigchild, NULL);

                // Send a signal to the child process to plot data
                kill(pid, SIGUSR1);

                for(EVER) {
                    sleep(1);
                }
            }
        }
    } else {
        // Child process

        // Sigint catcher, if called child process destroys
        // pipes and shared memory
        // Then ended itself which end parent process thanks to sigchld
        struct sigaction action;
        action.sa_handler = &handle_sigint;
        sigaction(SIGINT, &action, NULL);
        sigaction(SIGTERM, &action, NULL);

        // Child wait for sigusr signal to plot data
        struct sigaction action_sigusr;
        action_sigusr.sa_handler = &handle_sigusr;
        sigaction(SIGUSR1, &action_sigusr, NULL);
        for(EVER) {
            sleep(1);
        }
    }
    return 0;
}

