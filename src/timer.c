/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "city.h"
#include "shared_memory.h"
#include "pipes.h"

int main(int argc, char *argv[])
{

        int fd;
        if (argc != 2) exit(EXIT_FAILURE);

        // Wait to ensure that pipes and shared memory
        // have been created in epidemic_sim
        sleep(4);

        // Accessing shared memory
        city_t *shm = open_client();


        // Opening timer_pipe in write mode
        fd = open(TIMER_PIPE, O_WRONLY);

        if (fd == -1) {
                perror("Pipe isn't available");
        }

        for (EVER) {
                sleep(atoi(argv[1]));

                // Writing 1 for the simulation to go on, 0 for it to stop
                if (shm->time < 100) {
                        shm->time++;
                        write(fd, "1", 1);

                } else {
                        write(fd, "0", 1);
                        close(fd);
                        exit(EXIT_SUCCESS);
                }
        }
        return 0;
}