/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include "display_functions.h"

void display_map(char **map)
{
        int i;
        int j;

        printf("__________________________________________\n");
        printf("\t ----------- MAP ----------- \n");
        printf("__________________________________________\n");
        printf("  \t  0   1   2   3   4   5   6 \n\n");
        printf("  \t --- --- --- --- --- --- ---\n");
        for (i = 0; i < 7; i++) {
                printf("%d \t|", i);
                for (j = 0; j < 7; j++) {
                        printf(" %c |", map[i][j]);
                }
                printf("\t%d\n  \t --- --- --- --- --- --- ---\n", i);
        }
        printf("\n  \t  0   1   2   3   4   5   6 \n\n");
        printf("(vide): terrain vague    H: hopital    #: maison    "
               "C: caserne de");
        printf(" pompier\n");

        for (i = 0; i < 7; ++i) {
                free(map[i]);
        }
        free(map);
}

char **city_to_map(city_t city)
{
        char **map;
        int i;
        int j;

        map = (char **) malloc(7 * sizeof(char *));
        for (i = 0; i < 7; ++i) {
                map[i] = (char *) malloc(7 * sizeof(char));
        }

        for (i = 0; i < 7; i++) {
                for (j = 0; j < 7; j++) {
                        switch (city.map[i][j].field.field) {
                                case WASTELAND:
                                        map[i][j] = ' ';
                                        break;
                                case HOSPITAL:
                                        map[i][j] = 'H';
                                        break;
                                case HOUSE:
                                        map[i][j] = '#';
                                        break;
                                case FIREHOUSE:
                                        map[i][j] = 'C';
                                        break;
                                default:
                                        map[i][j] = '!';
                        }
                }
        }
        return map;
}

void display_city_all(city_t city)
{
        char **map;

        /*----- MAP -----*/
        map = city_to_map(city);
        printf("Time is %d\n", city.time);
        display_map(map);
}


void display_city_population(city_t city)
{
        printf("Time is %d\n", city.time);
        display_data(population_to_data_cell(city));
}

void display_city_parameters(city_t city)
{
        printf("Time is %d\n", city.time);
        display_data(city_parameters_to_data_cell(city));
}

void display_data(data_cell_t **data)
{
        int i;
        int j;
        int k;
        int l;

        printf("\n\t   0      1      2      3      4      5      6\n\n");
        printf("\t ------ ------ ------ ------ ------ ------ ------ \n");

        for (i = 0; i < 7; i++) {
                for (j = 0; j < 4; j++) {
                        if (j == 1) {
                                printf("%d ", i);
                        } else {
                                printf("  ");
                        }
                        printf("\t| ");
                        for (k = 0; k < 7; k++) {
                                for (l = 0; l < 4; l++) {
                                        printf("%c",
                                                data[i][k].lines[j][l]);
                                }
                                printf(" | ");
                        }
                        if (j == 1) {
                                printf("\t%d ", i);
                        }
                        printf("\n");
                }
                printf("\t ------ ------ ------ ------ ------ ------ "
                       "------ \n");
        }
        printf("\n\t   0      1      2      3      4      5      6\n");

        for (i = 0; i < MAP_SIZE; i++) {
                free(data[i]);
        }
        free(data);

}

data_cell_t **population_to_data_cell(city_t city)
{
        data_cell_t **data;
        int i;
        char *firefighters;
        char *doctors;
        char *reporters;
        char *civilians;

        data = (data_cell_t **) malloc(7 * sizeof(data_cell_t *));

        for (i = 0; i < MAP_SIZE; i++) {
                data[i] = (data_cell_t *) malloc(7 * sizeof(data_cell_t));
        }

        for (i = 0; i < MAP_SIZE; i++) {
                for (int j = 0; j < MAP_SIZE; j++) {
                        firefighters = parse_integer(
                                city.map[i][j].firefighters);
                        data[i][j].lines[0][0] = 'P';
                        data[i][j].lines[0][1] = ':';
                        data[i][j].lines[0][2] = firefighters[0];
                        data[i][j].lines[0][3] = firefighters[1];
                        free(firefighters);

                        doctors = parse_integer(city.map[i][j].doctors);
                        data[i][j].lines[1][0] = 'M';
                        data[i][j].lines[1][1] = ':';
                        data[i][j].lines[1][2] = doctors[0];
                        data[i][j].lines[1][3] = doctors[1];
                        free(doctors);

                        reporters = parse_integer(city.map[i][j].reporters);
                        data[i][j].lines[2][0] = 'J';
                        data[i][j].lines[2][1] = ':';
                        data[i][j].lines[2][2] = reporters[0];
                        data[i][j].lines[2][3] = reporters[1];
                        free(reporters);

                        civilians = parse_integer(city.map[i][j].civilians);
                        data[i][j].lines[3][0] = 'C';
                        data[i][j].lines[3][1] = ':';
                        data[i][j].lines[3][2] = civilians[0];
                        data[i][j].lines[3][3] = civilians[1];
                        free(civilians);
                }
        }

        return data;
}

char *parse_integer(int value)
{
        char *parsed_int;

        parsed_int = (char *) malloc(2 * sizeof(char));

        if (count_digits(value) == 0) {
                parsed_int[1] = '-';
                parsed_int[0] = ' ';
        } else if (count_digits(value) == 1) {
                parsed_int[1] = value + '0';
                parsed_int[0] = ' ';
        } else if (count_digits(value) == 2) {
                sprintf(parsed_int, "%d", value);
        } else {
                parsed_int[0] = '?';
                parsed_int[1] = '?';
        }
        return parsed_int;
}

int count_digits(int value)
{
        int digits;

        digits = 0;
        if (value == 0) {
                return digits;
        } else {
                while (value != 0) {
                        digits++;
                        value /= 10;
                }
                return digits;
        }
}

data_cell_t **city_parameters_to_data_cell(city_t city)
{
        data_cell_t **data;
        int i, k;
        char *field;
        char *population;
        float contamination_level;
        char contamination[10];

        data = (data_cell_t **) malloc(7 * sizeof(data_cell_t *));

        for (i = 0; i < MAP_SIZE; i++) {
                data[i] = (data_cell_t *) malloc(7 * sizeof(data_cell_t));
        }

        for (i = 0; i < MAP_SIZE; i++) {
                for (int j = 0; j < MAP_SIZE; j++) {
                        switch (city.map[i][j].field.field) {
                                case WASTELAND:
                                        field = "WASTELAND";
                                        break;
                                case HOSPITAL:
                                        field = "HOSPITAL";
                                        break;
                                case HOUSE:
                                        field = "DWELLING";
                                        break;
                                case FIREHOUSE:
                                        field = "FIREHOUSE";
                                        break;
                                default:
                                        field = "NONE";
                        }
                        for (k = 0; k < 4; ++k) {
                                data[i][j].lines[0][k] = field[k];
                                data[i][j].lines[3][k] = ' ';
                        }

                        population = parse_integer(
                                get_number_of_citizens(city.map[i][j]));
                        data[i][j].lines[1][0] = 'P';
                        data[i][j].lines[1][1] = ':';
                        data[i][j].lines[1][2] = population[0];
                        data[i][j].lines[1][3] = population[1];
                        free(population);

                        contamination_level = city.map[i][j].field
                                 .contamination_level;
                        if (contamination_level > 0) {
                                sprintf(contamination, "%f",
                                        contamination_level);
                                for (k = 0; k < 4; ++k) {
                                        data[i][j].lines[2][k] =
                                                contamination[k];
                                }
                        } else {
                                data[i][j].lines[2][0] = ' ';
                                data[i][j].lines[2][1] = '-';
                                data[i][j].lines[2][2] = '-';
                                data[i][j].lines[2][3] = ' ';

                        }
                }
        }
        return data;
}

