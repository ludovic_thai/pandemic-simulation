/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#include <display_shell/display_functions.h>
#include "shell.h"
#include "global.h"

WINDOW *init_places_map()
{
        WINDOW *box;
        // box = (WINDOW *) malloc(sizeof(WINDOW));

        attron(A_UNDERLINE);
        mvprintw(BASE_Y_PLACES - 2, BASE_X_PLACES + 8, "LOCATIONS MAP");
        attroff(A_UNDERLINE);

        box = subwin(stdscr, 16, 30, BASE_Y_PLACES, BASE_X_PLACES);
        start_color();
        init_pair(FORE_WHITE, COLOR_WHITE, COLOR_WHITE);
        wattron(box, COLOR_PAIR(FORE_WHITE));
        wborder(box, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        wattroff(box, COLOR_PAIR(FORE_WHITE));
        wrefresh(box);
        refresh();

        init_pair(DWELLING_COLOR, COLOR_BLUE, COLOR_BLUE);
        init_pair(HOSPITAL_COLOR, COLOR_MAGENTA, COLOR_MAGENTA);
        init_pair(WASTELAND_COLOR, COLOR_GREEN, COLOR_GREEN);
        init_pair(FIREHOUSE_COLOR, COLOR_RED, COLOR_RED);

        attron(A_UNDERLINE);
        mvprintw(BASE_Y_PLACES + 17, BASE_X_PLACES, "LEGEND :");
        attroff(A_UNDERLINE);

        attron(COLOR_PAIR(DWELLING_COLOR));
        mvprintw(BASE_Y_PLACES + 19, BASE_X_PLACES, "  ");
        attroff(COLOR_PAIR(DWELLING_COLOR));
        mvprintw(BASE_Y_PLACES + 19, BASE_X_PLACES + 3, "DWELLING");

        attron(COLOR_PAIR(HOSPITAL_COLOR));
        mvprintw(BASE_Y_PLACES + 21, BASE_X_PLACES, "  ");
        attroff(COLOR_PAIR(HOSPITAL_COLOR));
        mvprintw(BASE_Y_PLACES + 21, BASE_X_PLACES + 3, "HOSPITAL");

        attron(COLOR_PAIR(WASTELAND_COLOR));
        mvprintw(BASE_Y_PLACES + 23, BASE_X_PLACES, "  ");
        attroff(COLOR_PAIR(WASTELAND_COLOR));
        mvprintw(BASE_Y_PLACES + 23, BASE_X_PLACES + 3, "WASTELAND");

        attron(COLOR_PAIR(FIREHOUSE_COLOR));
        mvprintw(BASE_Y_PLACES + 25, BASE_X_PLACES, "  ");
        attroff(COLOR_PAIR(FIREHOUSE_COLOR));
        mvprintw(BASE_Y_PLACES + 25, BASE_X_PLACES + 3, "FIREHOUSE");

        refresh();


        return box;
}

void refresh_places_map(city_t *city, WINDOW *box)
{
        int i, j;
        field_type type;
        for (i = 0; i < MAP_SIZE; ++i) {
                for (j = 0; j < MAP_SIZE; ++j) {
                        type = city->map[i][j].field.field;
                        switch (type) {
                                case HOUSE :
                                        wattron(box,
                                                COLOR_PAIR(DWELLING_COLOR));
                                        mvwprintw(box, 1 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        mvwprintw(box, 2 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        wattroff(box,
                                                 COLOR_PAIR(DWELLING_COLOR));
                                        break;
                                case FIREHOUSE:
                                        wattron(box,
                                                COLOR_PAIR(FIREHOUSE_COLOR));
                                        mvwprintw(box, 1 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        mvwprintw(box, 2 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        wattroff(box,
                                                COLOR_PAIR(FIREHOUSE_COLOR));
                                        break;
                                case HOSPITAL:
                                        wattron(box,
                                                COLOR_PAIR(HOSPITAL_COLOR));
                                        mvwprintw(box, 1 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        mvwprintw(box, 2 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        wattroff(box,
                                                 COLOR_PAIR(HOSPITAL_COLOR));
                                        break;
                                case WASTELAND:
                                        wattron(box,
                                                COLOR_PAIR(WASTELAND_COLOR));
                                        mvwprintw(box, 1 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        mvwprintw(box, 2 + 2 * j, 1 + 4 * i,
                                                  "    ");
                                        wattroff(box,
                                                COLOR_PAIR(WASTELAND_COLOR));
                                        break;
                                case NONE:
                                        break;
                                default:
                                        break;
                        }
                }
        }
        wrefresh(box);
        refresh();
}

WINDOW *init_contamination_map()
{
        WINDOW *box;
        //box = (WINDOW *) malloc(sizeof(WINDOW));

        attron(A_UNDERLINE);
        mvprintw(BASE_Y_CONT - 2, BASE_X_CONT + 8, "CONTAMINATION");
        attroff(A_UNDERLINE);

        box = subwin(stdscr, 16, 30, BASE_Y_CONT, BASE_X_CONT);
        start_color();
        init_pair(FORE_WHITE, COLOR_WHITE, COLOR_WHITE);
        wattron(box, COLOR_PAIR(FORE_WHITE));
        wborder(box, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        wattroff(box, COLOR_PAIR(FORE_WHITE));
        wrefresh(box);
        refresh();

        init_pair(CONTAMINATION_LVL1, COLOR_CYAN, COLOR_CYAN);
        init_pair(CONTAMINATION_LVL2, COLOR_YELLOW, COLOR_YELLOW);
        init_pair(CONTAMINATION_LVL3, COLOR_BLUE, COLOR_BLUE);
        init_pair(CONTAMINATION_LVL4, COLOR_GREEN, COLOR_GREEN);
        init_pair(CONTAMINATION_LVL5, COLOR_RED, COLOR_RED);

        attron(A_UNDERLINE);
        mvprintw(BASE_Y_CONT + 17, BASE_X_CONT, "LEGEND :");
        attroff(A_UNDERLINE);

        attron(COLOR_PAIR(CONTAMINATION_LVL1));
        mvprintw(BASE_Y_CONT + 19, BASE_X_CONT, "  ");
        attroff(COLOR_PAIR(CONTAMINATION_LVL1));
        mvprintw(BASE_Y_CONT + 19, BASE_X_CONT + 3, "0 < CONTAMINATION < 10");

        attron(COLOR_PAIR(CONTAMINATION_LVL2));
        mvprintw(BASE_Y_CONT + 21, BASE_X_CONT, "  ");
        attroff(COLOR_PAIR(CONTAMINATION_LVL2));
        mvprintw(BASE_Y_CONT + 21, BASE_X_CONT + 3, "10 < CONTAMINATION < 20");

        attron(COLOR_PAIR(CONTAMINATION_LVL3));
        mvprintw(BASE_Y_CONT + 23, BASE_X_CONT, "  ");
        attroff(COLOR_PAIR(CONTAMINATION_LVL3));
        mvprintw(BASE_Y_CONT + 23, BASE_X_CONT + 3, "20 < CONTAMINATION < 40");

        attron(COLOR_PAIR(CONTAMINATION_LVL4));
        mvprintw(BASE_Y_CONT + 25, BASE_X_CONT, "  ");
        attroff(COLOR_PAIR(CONTAMINATION_LVL4));
        mvprintw(BASE_Y_CONT + 25, BASE_X_CONT + 3, "40 < CONTAMINATION < 60");

        attron(COLOR_PAIR(CONTAMINATION_LVL5));
        mvprintw(BASE_Y_CONT + 27, BASE_X_CONT, "  ");
        attroff(COLOR_PAIR(CONTAMINATION_LVL5));
        mvprintw(BASE_Y_CONT + 27, BASE_X_CONT + 3, "60 < CONTAMINATION < 100");


        refresh();


        return box;
}

void refresh_contamination_map(city_t *city, WINDOW *box)
{
        int i, j;
        float lvl;
        for (i = 0; i < MAP_SIZE; ++i) {
                for (j = 0; j < MAP_SIZE; ++j) {
                        lvl = city->map[i][j].field.contamination_level;
                        if ((lvl >= 0) && (lvl <= 0.1)) {
                                wattron(box,
                                        COLOR_PAIR(CONTAMINATION_LVL1));
                                mvwprintw(box, 1 + 2 * j, 1 + 4 * i, "    ");
                                mvwprintw(box, 2 + 2 * j, 1 + 4 * i, "    ");
                                wattroff(box,
                                        COLOR_PAIR(CONTAMINATION_LVL1));
                        } else if ((lvl > 0.1) && (lvl <= 0.2)) {
                                wattron(box,
                                        COLOR_PAIR(CONTAMINATION_LVL2));
                                mvwprintw(box, 1 + 2 * j, 1 + 4 * i, "    ");
                                mvwprintw(box, 2 + 2 * j, 1 + 4 * i, "    ");
                                wattroff(box,
                                        COLOR_PAIR(CONTAMINATION_LVL2));
                        } else if ((lvl > 0.2) && (lvl <= 0.4)) {
                                wattron(box,
                                        COLOR_PAIR(CONTAMINATION_LVL3));
                                mvwprintw(box, 1 + 2 * j, 1 + 4 * i, "    ");
                                mvwprintw(box, 2 + 2 * j, 1 + 4 * i, "    ");
                                wattroff(box,
                                        COLOR_PAIR(CONTAMINATION_LVL3));
                        } else if ((lvl > 0.4) && (lvl <= 0.6)) {
                                wattron(box,
                                        COLOR_PAIR(CONTAMINATION_LVL4));
                                mvwprintw(box, 1 + 2 * j, 1 + 4 * i, "    ");
                                mvwprintw(box, 2 + 2 * j, 1 + 4 * i, "    ");
                                wattroff(box,
                                        COLOR_PAIR(CONTAMINATION_LVL4));
                        } else if ((lvl > 0.6) && (lvl <= 1)) {
                                wattron(box,
                                        COLOR_PAIR(CONTAMINATION_LVL5));
                                mvwprintw(box, 1 + 2 * j, 1 + 4 * i, "    ");
                                mvwprintw(box, 2 + 2 * j, 1 + 4 * i, "    ");
                                wattroff(box,
                                        COLOR_PAIR(CONTAMINATION_LVL5));
                        }
                }
        }
        wrefresh(box);
        refresh();
}

WINDOW *init_population_map()
{
        WINDOW *box;
        //box = (WINDOW *) malloc(sizeof(WINDOW));

        attron(A_UNDERLINE);
        mvprintw(BASE_Y_POP - 2, BASE_X_POP + 3, "POPULATION");
        attroff(A_UNDERLINE);

        box = subwin(stdscr, 9, 16, BASE_Y_POP, BASE_X_POP);
        start_color();
        init_pair(FORE_WHITE, COLOR_WHITE, COLOR_WHITE);
        wattron(box, COLOR_PAIR(FORE_WHITE));
        wborder(box, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        wattroff(box, COLOR_PAIR(FORE_WHITE));
        wrefresh(box);
        refresh();


        return box;
}

void refresh_population_map(city_t *city, WINDOW *box)
{
        int i, j;
        for (i = 0; i < MAP_SIZE; ++i) {
                for (j = 0; j < MAP_SIZE; ++j) {
                        mvwprintw(box, 1 + j, 1 + 2 * i, "%d ",
                                  get_number_of_citizens(city->map[i][j]));
                }
        }
        wrefresh(box);
        refresh();
}

void refresh_repartition(city_t *city)
{
        attron(A_UNDERLINE);
        mvprintw(BASE_Y_POP + 12, BASE_X_POP, "REPARTITION");
        attroff(A_UNDERLINE);
        mvprintw(BASE_Y_POP + 14, BASE_X_POP, "HEALTHY : %d ",
                 number_of_healthy(*city));
        mvprintw(BASE_Y_POP + 16, BASE_X_POP, "SICK :    %d ",
                 number_of_sick(*city));
        mvprintw(BASE_Y_POP + 18, BASE_X_POP, "DEAD :    %d ",
                 number_of_dead(*city));
        mvprintw(BASE_Y_POP + 20, BASE_X_POP, "BURNED :  %d ",
                 number_of_burned(*city));
}

void refresh_shell(city_t *city)
{
        WINDOW *places_box;
        WINDOW *contamination_box;
        WINDOW *population_box;

        attron(A_BOLD | A_UNDERLINE);
        mvprintw(1, 70, "ROUND %d/100", city->time);
        attroff(A_BOLD | A_UNDERLINE);

        places_box = init_places_map();
        contamination_box = init_contamination_map();
        population_box = init_population_map();

        refresh_places_map(city, places_box);
        refresh_contamination_map(city, contamination_box);
        refresh_population_map(city, population_box);
        refresh_repartition(city);
        move(0, 0);
}