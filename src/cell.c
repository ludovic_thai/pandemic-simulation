/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "cell.h"
#include <stdio.h>

cell_t new_cell(coordinates_t coordinates)
{

        cell_t cell;
        cell.coordinates = coordinates;
        cell.field = new_field();
        cell.firefighters = 0;
        cell.doctors = 0;
        cell.reporters = 0;
        cell.civilians = 0;

        for (int i = 0; i < MAX_CAPACITY; ++i) {
                cell.ids[i] = -1;
        }

        return cell;
}


cell_t init_cell_field(cell_t cell, field_type type_of_field)
{
        cell.field = init_field(type_of_field);
        return cell;
}


void display_cell(cell_t cell)
{
        display_field_type(cell.field);
        printf(" (%d,%d)", cell.coordinates.pos_x,
                cell.coordinates.pos_y);
        printf("\tFirefighters: %d\n", cell.firefighters);
        printf("\tDoctors: %d\n", cell.doctors);
        printf("\tReporters: %d\n", cell.reporters);
        printf("\tCivilians: %d\n", cell.civilians);
}

int is_full(cell_t cell)
{
        if (get_number_of_citizens(cell) < cell.field.capacity) {
                return 0;
        } else {
                return 1;
        }
}

int get_number_of_citizens(cell_t cell)
{
        return cell.civilians + cell.doctors + cell.firefighters +
               cell.reporters;
}

