#include "coordinates.h"
#include "global.h"

coordinates_t new_coordinates(int x, int y)
{
        coordinates_t coordinates;
        coordinates.pos_x = x;
        coordinates.pos_y = y;
        return coordinates;
}

int equals(coordinates_t pos1, coordinates_t pos2)
{
        return (pos1.pos_x == pos2.pos_x) && (pos2.pos_y == pos1.pos_y);
}


int is_in_map_boundaries(coordinates_t pos)
{
        return ((pos.pos_x >= 0) && (pos.pos_y >= 0) &&
                (pos.pos_x < MAP_SIZE) && (pos.pos_y < MAP_SIZE));
}