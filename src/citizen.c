/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "citizen.h"
#include <stdio.h>

citizen_t new_citizen(citizen_type type, coordinates_t position, int id)
{
        citizen_t citizen;
        citizen.type = type;
        citizen.position = position;
        citizen.id = id;
        citizen.contamination_level = 0;
        citizen.status = HEALTHY;
        citizen.contamination_day = -1;
        citizen.healing_day = -1;
        citizen.last_entry_in_hospital = -1;
        citizen.death_probability = 0;

        switch (type) {
                case FIREFIGHTER:
                        citizen.spray_power = 5.0f;
                        citizen.care_package = 0;
                        break;
                case DOCTOR:
                        citizen.spray_power = 0;
                        citizen.care_package = 5;
                        break;
                default:
                        citizen.spray_power = 0;
                        citizen.care_package = 0;
                        break;
        }

        return citizen;
}

void display_citizen_type(citizen_t citizen)
{
        switch (citizen.type) {
                case DOCTOR:
                        printf("Doctor");
                        break;
                case FIREFIGHTER:
                        printf("Firefighter");
                        break;
                case REPORTER:
                        printf("Reporter");
                        break;
                case CIVILIAN:
                        printf("Civilian");
                        break;
        }
}

void display_life_status(citizen_t citizen)
{
        switch (citizen.status) {
                case DEAD:
                        printf("DEAD");
                        break;
                case HEALTHY:
                        printf("HEALTHY");
                        break;
                case SICK:
                        printf("SICK");
                        break;
                case BURNED:
                        printf("BURNED");
                        break;
        }
}

void display_skills(citizen_t citizen)
{
        switch (citizen.type) {
                case FIREFIGHTER:
                        printf("Spray power: %f\n", citizen.spray_power);
                        break;
                case DOCTOR:
                        printf("Care package: %d\n",
                                citizen.care_package);
                        break;
                default:
                        break;
        }
}

void display_citizen(citizen_t citizen)
{
        display_citizen_type(citizen);
        printf(": ID: %d\t", citizen.id);
        display_life_status(citizen);
        printf("\n\tPosition: (%d,%d)\n", citizen.position.pos_x,
               citizen.position.pos_y);
        printf("\tContamination level: %f\n",
                citizen.contamination_level);
        printf("\tContamination day: %d\n", citizen.contamination_day);
        printf("\tHealing day: %d\n", citizen.healing_day);
        printf("\tLast day entry in hospital: %d\n",
               citizen.last_entry_in_hospital);
        printf("\tDeath probability: %f\n", citizen.death_probability);
        printf("\t");
        display_skills(citizen);
        printf("\n");
}