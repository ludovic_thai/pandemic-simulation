/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <unistd.h>
#include "city.h"


city_t new_city()
{
        int i;
        city_t city;
        for (i = 0; i < MAP_SIZE; i++) {
                for (int j = 0; j < MAP_SIZE; j++) {
                        cell_t cell = new_cell(new_coordinates(i, j));
                        city.map[i][j] = cell;
                }
        }
        city.time = 0;

        return city;
}

void display_city(city_t city)
{
        int i, j;
        printf("Map at time %d :\n", city.time);
        printf("===============================================\n");

        for (i = 0; i < MAP_SIZE; i++) {
                for (j = 0; j < MAP_SIZE; j++) {
                        display_cell(city.map[i][j]);
                        printf("======================================"
                               "=========\n");
                }
        }
}

city_t place_hospital(city_t city)
{
        city.map[MAP_SIZE / 2][MAP_SIZE / 2].field = init_field(HOSPITAL);
        return city;
}

city_t place_firehouses(city_t city)
{
        city.map[0][MAP_SIZE - 1].field = init_field(FIREHOUSE);
        city.map[MAP_SIZE - 1][0].field = init_field(FIREHOUSE);
        return city;
}

city_t place_houses(city_t city, int houses)
{
        srand(time(NULL));

        for (int i = 0; i < houses; i++) {
                int row = rand() % MAP_SIZE;
                int col = rand() % MAP_SIZE;
                if (city.map[row][col].field.field == NONE) {
                        city.map[row][col].field = init_field(HOUSE);
                } else {
                        i--;
                }
        }
        return city;
}

city_t place_wastelands(city_t city)
{
        for (int i = 0; i < MAP_SIZE; i++) {
                for (int j = 0; j < MAP_SIZE; j++) {
                        if (city.map[i][j].field.field == NONE) {
                                city.map[i][j].field = init_field(WASTELAND);
                        }
                }
        }
        return city;
}

city_t init_fields(city_t city, int houses)
{
        city = place_hospital(city);
        city = place_firehouses(city);
        city = place_houses(city, houses);
        city = place_wastelands(city);
        return city;
}

city_t place_citizens(city_t city, int nb_doctors, int nb_firefighters,
                      int nb_civilians, int nb_reporters)
{
        srand(time(NULL));
        int id;
        int i;
        int x, y;
        int size;
        id = 0;
        size = MAP_SIZE;
        city.citizens[id] = new_citizen(DOCTOR,
                new_coordinates(size / 2, size / 2),
                                        id);
        city = add_citizen_to_city(city, city.map[size / 2][size / 2],
                                   city.citizens[id]);
        id++;
        city.citizens[id] = new_citizen(FIREFIGHTER,
                new_coordinates(0, size - 1), id);
        city = add_citizen_to_city(city, city.map[0][size - 1],
                                   city.citizens[id]);
        id++;
        city.citizens[id] = new_citizen(FIREFIGHTER,
                new_coordinates(size - 1, 0), id);
        city = add_citizen_to_city(city, city.map[size - 1][0],
                                   city.citizens[id]);
        id++;
        for (i = 0; i < nb_doctors - 1; i++) {
                x = rand() % size;
                y = rand() % size;
                if (!is_full(city.map[x][y])) {
                        city.citizens[id] = new_citizen(DOCTOR,
                                                        new_coordinates(x, y),
                                                        id);
                        city = add_citizen_to_city(city, city.map[x][y],
                                                   city.citizens[id]);
                        id++;
                } else {
                        i--;
                }
        }
        for (i = 0; i < nb_firefighters - 2; i++) {
                x = rand() % size;
                y = rand() % size;
                if (!is_full(city.map[x][y])) {
                        city.citizens[id] = new_citizen(FIREFIGHTER,
                                                        new_coordinates(x, y),
                                                        id);
                        city = add_citizen_to_city(city, city.map[x][y],
                                                   city.citizens[id]);
                        id++;
                } else {
                        i--;
                }
        }
        for (i = 0; i < nb_civilians; i++) {
                x = rand() % size;
                y = rand() % size;
                if (!is_full(city.map[x][y])) {
                        city.citizens[id] = new_citizen(CIVILIAN,
                                                        new_coordinates(x, y),
                                                        id);
                        city = add_citizen_to_city(city, city.map[x][y],
                                                   city.citizens[id]);
                        id++;
                } else {
                        i--;
                }
        }
        for (i = 0; i < nb_reporters; i++) {
                x = rand() % size;
                y = rand() % size;
                if (!is_full(city.map[x][y])) {
                        city.citizens[id] = new_citizen(REPORTER,
                                                        new_coordinates(x, y),
                                                        id);
                        city = add_citizen_to_city(city, city.map[x][y],
                                                   city.citizens[id]);
                        id++;
                } else {
                        i--;
                }
        }

        return city;
}

city_t add_citizen_to_city(city_t city, cell_t cell, citizen_t citizen)
{
        if (is_full(cell) == 0) {
                citizen.position.pos_x = cell.coordinates.pos_x;
                citizen.position.pos_y = cell.coordinates.pos_y;
                switch (citizen.type) {
                        case DOCTOR:
                                cell.doctors++;
                                break;
                        case FIREFIGHTER:
                                cell.firefighters++;
                                break;
                        case REPORTER:
                                cell.reporters++;
                                break;
                        case CIVILIAN:
                                cell.civilians++;
                                break;
                }
                for (int i = 0; i < cell.field.capacity; i++) {
                        if (cell.ids[i] == -1) {
                                cell.ids[i] = citizen.id;
                                break;
                        }
                }
                city.map[cell.coordinates.pos_x][cell.coordinates.pos_y] = cell;
                return city;
        }
        exit(EXIT_FAILURE);
}

city_t init_city()
{
        city_t city = new_city();
        city = init_fields(city, NUMBER_OF_HOUSES);
        city = place_citizens(city, NUMBER_OF_DOCTORS, NUMBER_OF_FIREFIGHTERS,
                              NUMBER_OF_CIVILIANS, NUMBER_OF_REPORTERS);
        city = init_wasteland_contamination(city, NUMBER_OF_WASTELANDS);

        // Init general informations
        city.general_informations.contaminated_citizens = 0;
        city.general_informations.average_contamination_level = 0.0f;
        city.general_informations.death_citizens = 0;
        return city;
}

city_t init_wasteland_contamination(city_t city, int nb_wastelands)
{
        int remaining_wasteland_to_contaminates;
        int x;
        int y;
        int counter;

        counter = 0;
        remaining_wasteland_to_contaminates = nb_wastelands / 10;

        srand(time(NULL));
        while (remaining_wasteland_to_contaminates > 0) {
                counter++;
                x = rand() % MAP_SIZE;
                y = rand() % MAP_SIZE;

                if (city.map[x][y].field.field == WASTELAND &&
                    city.map[x][y].field.contamination_level == 0.00) {
                        city.map[x][y].field = init_contamination_level(
                                city.map[x][y].field);
                        sleep(1);
                        remaining_wasteland_to_contaminates--;
                }
        }
        return city;
}

float min2(float num1, float num2)
{
        if (num1 < num2) {
                return num1;
        } else {
                return num2;
        }
}

city_t decontaminate_citizen(city_t city)
{
        for (int i = 0; i < MAP_SIZE; ++i) {
                for (int j = 0; j < MAP_SIZE; ++j) {
                        if (city.map[i][j].field.field == HOSPITAL) {
                                for (int k = 0; k < POPULATION; ++k) {
                                        if (equals(city.citizens[k].position,
                                                   new_coordinates(i, j)) &&
                                            city.citizens[k].contamination_level
                                            > city.map[i][j].field.
                                            contamination_level) {
                                                city.citizens[k].
                                                contamination_level -= min2(
                                                        0.1f, min2(
                                                                city.citizens[k]
                                                                .contamination_level,
                                                                city.
                                                                citizens[k].
                                                                contamination_level -
                                                                city.map[i][j].
                                                                field.
                                                                contamination_level));
                                        }
                                }
                        } else if (city.map[i][j].field.field == FIREHOUSE) {
                                for (int k = 0; k < POPULATION; ++k) {
                                        if (equals(city.citizens[k].position,
                                                   new_coordinates(i,
                                                                   j))) {
                                                city.citizens[k].
                                                contamination_level -= min2(
                                                        0.2f,
                                                        city.citizens[k].
                                                        contamination_level);
                                        }
                                }
                        }
                }
        }
        return city;
}

city_t wind_driven_propagation(city_t city)
{
        int i, j;
        for (i = 0; i < MAP_SIZE; ++i) {
                for (j = 0; j < MAP_SIZE; ++j) {
                        if (city.map[i][j].field.field == WASTELAND) {
                                city = update_neighbours_contamination(city,
                                        new_coordinates(i, j));
                        }
                }
        }
        return city;
}

city_t update_neighbours_contamination(city_t city, coordinates_t coordinates)
{
        srand(time(NULL));
        int x, y;
        int i, j;
        int size = MAP_SIZE;
        float contamination_difference;
        x = coordinates.pos_x;
        y = coordinates.pos_y;
        for (i = -1; i <= 1; i++) {
                for (j = -1; j <= 1; j++) {
                        if ((x + i < size) && (x + i >= 0) && (y + j < size) &&
                            (y + j >= 0) &&
                            (city.map[x + i][y + j].field.field == WASTELAND)) {
                                contamination_difference =
                                        city.map[x][y].field.contamination_level
                                        -city.map[x + i][y + j].field
                                        .contamination_level;
                                if (contamination_difference > 0) {
                                        if (rand() % 100 < 15) {
                                                city = increase_contamination(
                                                        city,
                                                        new_coordinates(x + i,
                                                                y + j),
                                                        contamination_difference);
                                        }
                                }
                        }
                }
        }
        return city;
}

city_t increase_contamination(city_t city, coordinates_t coordinates,
                              float contamination_difference)
{
        srand(time(NULL));
        int x = rand();
        int i = coordinates.pos_x;
        int j = coordinates.pos_y;
        float increase = (float) ((x % 20 + 1) * contamination_difference /
                                  100);
        city.map[i][j].field.contamination_level += increase;
        if (city.map[i][j].field.contamination_level > 1) {
                city.map[i][j].field.contamination_level = 1;
        }
        return city;
}


int number_of_healthy(city_t city)
{
        int healthy = 0;
        int j;
        for (j = 0; j < POPULATION; ++j) {
                if (city.citizens[j].status == HEALTHY) {
                        healthy++;
                }
        }
        return healthy;
}

int number_of_sick(city_t city)
{
        int sick = 0;
        int j;
        for (j = 0; j < POPULATION; ++j) {
                if (city.citizens[j].status == SICK) {
                        sick++;
                }
        }
        return sick;
}

int number_of_dead(city_t city)
{
        int dead = 0;
        int j;
        for (j = 0; j < POPULATION; ++j) {
                if (city.citizens[j].status == DEAD) {
                        dead++;
                }
        }
        return dead;
}

int number_of_burned(city_t city)
{
        int burned = 0;
        int j;
        for (j = 0; j < POPULATION; ++j) {
                if (city.citizens[j].status == BURNED) {
                        burned++;
                }
        }
        return burned;
}

float average_contamination_level(city_t city)
{
        float total_contamination_level;
        int i;

        total_contamination_level = 0.0;
        for (i = 0; i < POPULATION; i++) {
                total_contamination_level = total_contamination_level
                                            +
                                            city.citizens[i].
                                            contamination_level;
        }

        return (total_contamination_level / POPULATION);
}

void update_general_informations(city_t *city)
{
        city->general_informations.contaminated_citizens =
                number_of_sick(*city);
        city->general_informations.average_contamination_level =
                average_contamination_level(*city);
        city->general_informations.death_citizens =
                number_of_dead(*city) + number_of_burned(*city);
}
