/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <pthread.h>
#include "shared_memory.h"
#include "city.h"
#include "citizen_threads.h"

int main()
{
        // Wait to ensure that pipes and shared memory
        // have been created in epidemic_sim
        sleep(4);
        int i;
        pthread_t t[POPULATION];
        thread_param_t param[POPULATION];

        // Accessing shared memory
        city_t *shm = open_client();

        // Creating threads
        for (i = 0; i < POPULATION; ++i) {
                param[i].city = shm;
                param[i].citizen_id = i;
                pthread_create(&t[i], NULL, &handle_citizen, &param[i]);
        }

        // Waiting for threads to end
        for (i = 0; i < POPULATION; ++i) {
                pthread_join(t[i], NULL);
        }

}