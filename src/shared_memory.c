/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "shared_memory.h"

/**
 * Handles a fatal error. It displays a message, then exits.
 */
void handle_error(char *message)
{
        fprintf(stderr, "%s", message);
        exit(EXIT_FAILURE);
}


city_t *open_server()
{
        int memory_descriptor;
        size_t memory_size = (1 * sizeof(city_t));
        city_t *shm;

        memory_descriptor = shm_open(
                MEMORY_PATH,
                O_CREAT | O_RDWR,
                S_IRWXU | S_IRWXG);
        if (memory_descriptor < 0) {
                handle_error("Error calling shm_open\n");
        }

        fprintf(stderr, "Shared memory object %s has been created\n",
                MEMORY_PATH);

        ftruncate(memory_descriptor, memory_size);

        shm = (city_t *) mmap(
                NULL,
                memory_size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                memory_descriptor,
                0);
        if (shm == MAP_FAILED) {
                handle_error("Error calling mmap\n");;
        }
        fprintf(stderr, "Memory of %zu bytes allocated.\n", memory_size);

        *shm = init_city();

        return shm;
}

city_t *open_client()
{
        int memory_descriptor;
        size_t memory_size = (1 * sizeof(city_t));
        city_t *shm;

        memory_descriptor = shm_open(
                MEMORY_PATH,
                O_RDWR,
                S_IRWXU | S_IRWXG);
        if (memory_descriptor < 0) {
                handle_error("Error calling shm_open\n");
        }

        fprintf(stderr, "Shared memory object %s has been opened\n",
                MEMORY_PATH);

        if (MAC_OSX) { /* MAC OS X abnormality about ftruncate */
                struct stat mapstat;
                if (-1 != fstat(memory_descriptor, &mapstat)
                    && mapstat.st_size == 0) {
                        ftruncate(memory_descriptor, memory_size);
                }
        } else {
                ftruncate(memory_descriptor, memory_size);
        }

        shm = (city_t *) mmap(
                NULL,
                memory_size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                memory_descriptor,
                0);
        if (shm == MAP_FAILED) {
                handle_error("Error calling mmap\n");
        }
        fprintf(stderr, "Shared memory of %zu bytes has been allocated\n",
                memory_size);

        return shm;
}


void unlink_memory()
{
        if (shm_unlink(MEMORY_PATH) == -1) {
                fprintf(stderr, "Error while suppressing memory ");
                perror(" ");
                exit(EXIT_FAILURE);
        } else {
                printf("Shared memory has been unlinked\n");
        }
}
