/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#include "pipes.h"

void kill_pipe(char *name)
{
        if (unlink(name) == -1) {
                if (errno != ENOENT) {
                        fprintf(stderr, "Error while suppressing "
                                        "pipe '%s'",name);
                        perror(" ");
                        exit(EXIT_FAILURE);
                }
        } else {
                printf("Pipe named %s has been destroyed\n", name);
        }
}