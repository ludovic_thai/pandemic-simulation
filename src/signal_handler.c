/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */


#include "signal_handler.h"

/**
 * Unlink the shared memory when receiving the SIGINT signal.
 */
void handle_sigint(int signum)
{
        endwin();
        printf("Signal %d has been catched\n", signum);
        kill_pipe(TIMER_PIPE);
        kill_pipe(SIM_PIPE);
        unlink_memory();
        exit(EXIT_SUCCESS);
}

void handle_sigusr(int signum)
{
        printf("Signal %d has been catched, child "
               "process is now ploting data\n",
               signum);
        // Plot data and end child process
        execl("/usr/bin/gnuplot", "gnuplot", "-persist", "commands.gp",
              NULL);
        exit(EXIT_SUCCESS);
}

void handle_sigchld(int signum)
{
        printf("Signal %d has been catched, parent process "
               "is now going to end itself\n",
               signum);
        // Destroy pipes and shm because time has expired
        unlink_memory();
        kill_pipe(TIMER_PIPE);
        kill_pipe(SIM_PIPE);
        // Parent process can now be ended because child's one has been ended
        exit(EXIT_SUCCESS);
}