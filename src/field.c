/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <stdlib.h>
#include <stdio.h>
#include "field.h"
#include "global.h"
#include <time.h>

field_t new_field()
{
        field_t field;

        field.capacity = 0;
        field.contamination_level = 0;
        field.field = NONE;

        return field;
}

field_t init_field(field_type type_of_field)
{
        field_t field;
        field.capacity = field_capacity(type_of_field);
        field.field = type_of_field;
        field.contamination_level = 0;
        return field;
}

void display_field(field_t field)
{
        display_field_type(field);
        printf("\n\t field capacity: %d", field.capacity);
        printf("\n\t field contamination level: %f\n",
               field.contamination_level);

}

void display_field_type(field_t field)
{
        switch (field.field) {
                case HOSPITAL:
                        printf("HOSPITAL");
                        break;
                case FIREHOUSE:
                        printf("FIREHOUSE");
                        break;
                case WASTELAND:
                        printf("WASTELAND");
                        break;
                case HOUSE:
                        printf("HOUSE");
                        break;
                case NONE:
                        printf("NONE");
        }

}

int field_capacity(field_type type_of_field)
{
        switch (type_of_field) {
                case FIREHOUSE:
                        return FIREHOUSE_CAPACITY;
                case HOSPITAL:
                        return HOSPITAL_CAPACITY;
                case HOUSE:
                        return HOUSE_CAPACITY;
                case WASTELAND:
                        return WASTELAND_CAPACITY;
                default:
                        exit(EXIT_FAILURE);
        }
}

field_t init_contamination_level(field_t field)
{
        srand(time(NULL));
        field.contamination_level = (float) (rand() % 20 + 20) / 100;
        return field;
}
