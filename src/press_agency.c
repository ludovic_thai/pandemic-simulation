/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <mqueue.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include "news.h"

int main()
{
        mqd_t mq;
        char buffer[MAX_SIZE + 1];
        int must_stop;

        must_stop = 0;
        mq = mq_open(MQUEUE_NAME, O_RDONLY);

        if ((mqd_t) -1 != mq) {
                fprintf(stderr, "Memory queue %s has been opened\n\n",
                        MQUEUE_NAME);
                printf(" ----- PRESS AGENCY ----- \n\n");
                while (!must_stop) {
                        ssize_t bytes_read;

                        //receive the message
                        bytes_read = mq_receive(mq, buffer,
                                MAX_SIZE, NULL);
                        if (bytes_read >= 0) {
                                buffer[bytes_read] = '\0';
                                //printf("%s \n", buffer);
                                message_t message;
                                message = parse_message(buffer);
                                display_message(message);
                        }
                }
        } else {
                fprintf(stderr, "Error openning memory queue : %s \n",
                        strerror(errno));
        }

        exit(EXIT_SUCCESS);
}
