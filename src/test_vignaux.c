/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <citizen.h>
#include "assert.h"
#include <city.h>
#include <global.h>

void test_new_citizen()
{
        citizen_t citizen = new_citizen(FIREFIGHTER,
                new_coordinates(5, 3), 4);
        assert(citizen.type == FIREFIGHTER);
        assert(citizen.position.pos_x == 5 && citizen.position.pos_y);
        assert(citizen.death_probability == 0);
        assert(citizen.last_entry_in_hospital == -1);
        assert(citizen.healing_day == -1);
        assert(citizen.contamination_day == -1);
        assert(citizen.status == HEALTHY);
        assert(citizen.id == 4);
        assert(citizen.care_package == 0);
        assert(citizen.spray_power == 5.0f);
        display_citizen(citizen);
}

/*void test_move_citizen() {
    city_t city;
    city = init_city();
    cell_t cell1 = city.map[5][4];
    cell_t cell2 = city.map[2][3];
    citizen_t citizen1 = new_citizen(FIREFIGHTER, new_coordinates(5,4), 1);
    citizen_t citizen2 = new_citizen(DOCTOR, new_coordinates(5,4), 2);
    citizen_t citizen3 = new_citizen(REPORTER, new_coordinates(2,3), 3);
    citizen_t citizen4 = new_citizen(CIVILIAN, new_coordinates(2,3), 4);
    add_citizen(city, cell1, citizen1);
    add_citizen(city, cell1, citizen2);
    add_citizen(city, cell2, citizen3);
    add_citizen(city, cell2, citizen4);
    display_cell(cell1);
    display_cell(cell2);
    move_one_citizen(city, cell1, cell2, citizen2);
    printf("===========================================\n");
    display_cell(cell1);
    display_cell(cell2);
}*/

void test_init_city_field()
{
        city_t city = new_city(MAP_SIZE);
        init_fields(city, NUMBER_OF_HOUSES);
        display_city(city);
}


int main()
{
        //test_new_citizen();
        //test_move_citizen();
        //test_move_citizen();
        //test_init_city_field();
}