/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include "general_informations.h"
#include <stdlib.h>

general_informations_t *new_general_informations()
{
        general_informations_t *general_Informations = (general_informations_t *)
                malloc(sizeof(general_informations_t));
        general_Informations->average_contamination_level = 0;
        general_Informations->contaminated_citizens = 0;
        general_Informations->death_citizens = 0;
        return general_Informations;
}
