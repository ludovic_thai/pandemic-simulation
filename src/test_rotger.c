//
// Created by rotger on 30/11/2019.
//


#include "shell.h"
#include "city.h"

int main()
{
        char c;
        initscr();
        cbreak();
        noecho();
        city_t city = init_city();
        refresh_shell(&city);
        while ((c = getch()) != 'q') {
                city.time++;
                city = wind_driven_propagation(city);
                refresh_shell(&city);
        }
        endwin();
        return 0;
}