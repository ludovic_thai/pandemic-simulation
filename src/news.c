/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students. No portion of this
 * document may be reproduced, copied or revised without written
 * permission of the authors.
 */

/*
 * PROJECT : OS - Plague Simulation
 *
 * @author Olivier ROTGER - orotger@ecole.ensicaen.fr
 * @author Ludovic THAI - ludovic.thai@ecole.ensicaen.fr
 * @author Benjamin VIGNAUX - bvignaux@ecole.ensicaen.fr
 */

#include <mqueue.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "news.h"

mqd_t new_mqueue()
{
        mqd_t mq;
        struct mq_attr attr;

        // Initializes the queue attributes
        attr.mq_flags = 0; //O_NONBLOCK;
        attr.mq_maxmsg = MAX_MESSAGE;
        attr.mq_msgsize = MAX_SIZE;
        attr.mq_curmsgs = 0;

        // Create the message queue
        mq = mq_open(MQUEUE_NAME, O_CREAT | O_RDONLY, 0644, &attr);
        if ((mqd_t) -1 == mq) {
                fprintf(stderr, "Error creating memory queue: %s\n",
                        strerror(errno));
        } else {
                fprintf(stderr, "Memory queue %s has been created\n",
                        MQUEUE_NAME);
        }
        if (mq_getattr(mq, &attr) == -1) {
                perror("mq_getattr");
        }
        return mq;
}

void send_reports(thread_param_t *param, citizen_t citizen)
{
        mqd_t mq;

        int total_deaths;
        float average_contamination_level;
        int citizen_contaminated;
        int time;

        time = param->city->time;
        total_deaths = param->city->general_informations.death_citizens;
        average_contamination_level = param->city->
                general_informations.average_contamination_level;
        citizen_contaminated = param->city->general_informations.
                contaminated_citizens;

        mq = mq_open(MQUEUE_NAME, O_WRONLY);
        if ((mqd_t) -1 != mq) {
                char m_average_contamination_level[MAX_SIZE];
                char m_contaminated[MAX_SIZE];
                char m_total_deaths[MAX_SIZE];
                char m_reporter_contamination_level[MAX_SIZE];

                snprintf(m_average_contamination_level, MAX_SIZE,
                         "%d,%d,avg,%f", time, citizen.id,
                         average_contamination_level);
                if (0 >
                    mq_send(mq, m_average_contamination_level,
                            MAX_SIZE, 5)) {
                        fprintf(stderr, "Error mq_send(): %s\n",
                                strerror(errno));
                }

                snprintf(m_contaminated, MAX_SIZE,
                         "%d,%d,sick,%d", time, citizen.id,
                         citizen_contaminated);
                if (0 > mq_send(mq, m_contaminated, MAX_SIZE, 2)) {
                        fprintf(stderr, "Error mq_send(): %s\n",
                                strerror(errno));
                }

                snprintf(m_total_deaths, MAX_SIZE,
                         "%d,%d,deaths,%d", time, citizen.id,
                         total_deaths);
                if (0 > mq_send(mq, m_total_deaths, MAX_SIZE, 10)) {
                        fprintf(stderr, "Error mq_send(): %s\n",
                                strerror(errno));
                }

                snprintf(m_reporter_contamination_level, MAX_SIZE,
                         "%d,%d,report,%f",
                         time, citizen.id, citizen.contamination_level);
                if (0 >
                    mq_send(mq, m_reporter_contamination_level,
                            MAX_SIZE, 1)) {
                        fprintf(stderr, "Error mq_send(): %s\n",
                                strerror(errno));
                }

                mq_close(mq);
        } else {
                fprintf(stderr, "Error mq_open(): %s\n", strerror(errno));
        }
}

message_t parse_message(char *message)
{
        message_t informations;
        char **tokens;

        tokens = str_split(message, ',');
        if (tokens) {
                // day
                informations.day = strtol(*(tokens), NULL, 10);
                free(*(tokens));

                // reporter ID
                informations.reporter_id = strtol(*(tokens + 1),
                        NULL, 10);
                free(*(tokens + 1));

                // messge type
                char message_type[50];
                snprintf(message_type, sizeof(*(tokens + 2)), "%s",
                         *(tokens + 2));
                free(*(tokens + 2));

                if (strstr(message_type, "avg") != NULL) {
                        informations.message_type = AVG_CONT_LVL;
                } else if (strstr(message_type, "sick") != NULL) {
                        informations.message_type = NB_SICK;
                } else if (strstr(message_type, "deaths") != NULL) {
                        informations.message_type = DEATHS;
                } else if (strstr(message_type, "report") != NULL) {
                        informations.message_type = REPORTER_CONT_LVL;
                } else {
                        informations.message_type = ERROR;
                }

                // value
                informations.value = strtof(*(tokens + 3), NULL);
                free(*(tokens + 3));

                free(tokens);
        }

        return informations;
}

char **str_split(char *a_str, const char a_delim)
{
        char **result = 0;
        size_t count = 0;
        char *tmp = a_str;
        char *last_comma = 0;
        char delim[2];
        delim[0] = a_delim;
        delim[1] = 0;

        /* Count how many elements will be extracted. */
        while (*tmp) {
                if (a_delim == *tmp) {
                        count++;
                        last_comma = tmp;
                }
                tmp++;
        }

        /* Add space for trailing token. */
        count += last_comma < (a_str + strlen(a_str) - 1);

        /* Add space for terminating null string so caller
           knows where the list of returned strings ends. */
        count++;

        result = malloc(sizeof(char *) * count);

        if (result) {
                size_t idx = 0;
                char *token = strtok(a_str, delim);

                while (token) {
                        assert(idx < count);
                        *(result + idx++) = strdup(token);
                        token = strtok(0, delim);
                }
                assert(idx == count - 1);
                *(result + idx) = 0;
        }

        return result;
}

void display_message(message_t message)
{
        switch (message.message_type) {
                case (AVG_CONT_LVL):
                        printf("Day %ld - "
                               "Average level of contamination : %f\n",
                               message.day, message.value);
                        break;
                case (NB_SICK):
                        printf("Day %ld - "
                               "Number of contaminated (sick) citizen : %d\n",
                               message.day, (int) (message.value * 0.90));
                        break;
                case (DEATHS):
                        printf("Day %ld - Total number of deaths : %d\n",
                               message.day, (int) (message.value * 0.65));
                        break;
                case (REPORTER_CONT_LVL):
                        if (message.value >= 0.8) {
                                printf("Day %ld - "
                                       "Reporter contamination level "
                                       "(id. %ld) : %f\n",
                                       message.day, message.reporter_id,
                                       message.value * 0.9);
                        }
                        break;
                case (ERROR):
                        printf("Erreur\n");
        }
}
